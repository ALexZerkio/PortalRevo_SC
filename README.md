### 传送门：革命 简体中文补丁

---

![Portal: Revolution](./content/img/RevoSC_title.png)

#### 介绍

传送门：革命 是一个刚刚推出不久的优质传送门社区模组，其剧情优秀、玩法新颖（引入了很多原版废弃的测试机制）等特点立刻吸引了不少传送门系列的新老玩家们。

但美中不足的是其并没有支持中文游戏字幕，毫无疑问这加大了玩家们对理解剧情所需的难度。

所以我们花了些两天时间肝制出了这份包含主界面汉化在内的中文补丁。希望能借此填补官方所缺失的中文体验（目前仅支持简体中文哦）。

#### 下载

# **注：最新构建包存放位置已迁移至 [PortalRevo SC release](https://gitlab.com/ALexZerkio/PortalRevo_SC-release)。**

如果您从未使用过 GitLab 等代码托管网站。请按如下操作下载补丁包（以目前最新 Release 示范）：

![Step 1](./content/img/d1.png)

![Step 2](./content/img/d2.png)

#### 安装

**注：最新构建包存放位置已迁移至 [PortalRevo SC release](https://gitlab.com/ALexZerkio/PortalRevo_SC-release)。**

1. 下载或 git clone 最新构建版本的补丁包到您的计算机。如何下载见上图。

![Download](./content/img/d3.png)

2. 解压 PortalRevo_SC_latest.tar。

![](./content/img/tar.png)

3. 在 Steam - Portal Revolution - 浏览本地文件，打开游戏根目录。

![Game dir](./content/img/homedir.png)

4. 将“revolution”目录替换至 Portal Revolution 游戏根目录。

![Replace](./content/img/replace.png)

5. **在 Steam - Portal Revolution - 属性，添加启动项**

![Properties](https://gitlab.com/ALexZerkio/PortalRevo_SC/-/raw/main/content/img/s2.png)

![Options](https://gitlab.com/ALexZerkio/PortalRevo_SC/-/raw/main/content/img/s3.png)

6. 开始游戏，享受中文化的传送门: 革命。

---

#### 支持我们

爱发电：https://afdian.net/a/485107C

Bilibili：https://space.bilibili.com/431870933

粉丝群[三日七隔]：707093465(QQ)
